<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Usuario;
use App\Form\ModifyFormType;
use App\Form\NewUserFormType;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserController extends AbstractController
{
    /**
     * @Route("/")
     */

    public function home()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        
        return $this->redirectToRoute('app_login');
    }

    /**
    * @Route("/new", name="create_userForm")
    */

    public function newUser(Request $request, 
        EntityManagerInterface $em,
        UserManager $um,
        UserPasswordEncoderInterface $passwordEncoder)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!$auth){
            $this->addFlash(
                'notice',
                'You must be login!!!'
            );
            return $this->redirectToRoute('app_login');
        }
        $usuario = new Usuario();
        $form = $this->createForm(NewUserFormType::class, $usuario);
        $form->handleRequest($request);
        $data = $form->getData();
        if($form->isSubmitted() && $form->isValid())
        {
            try{
                $data = $form->getData();
                $userExist = $um->checkIfUserExist($data->getUsername(), $em);
                if($userExist) //IF USER EXIST RETURN A MESSAGE AND REDIRECT TO CREATE FORM
                {
                    $this->addFlash(
                        'notice',
                        'The user already exist!'
                    );
                    return $this->redirectToRoute('create_userForm');
                }
                //IF USER DON'T EXIST CONTINUES
                $password = $data->getPassword();
                $usuario->setPassword($passwordEncoder->encodePassword($usuario, $password));
                $usuario->setEmail($data->getEmail());
                $usuario->setRoles(['ROLE_USER']);
                $usuario->setComments($data->getComments());
                $em->persist($usuario);
                $em->flush();
                return $this->redirectToRoute('users_list');
            }catch(Exception $e)
            {
               dd($e->getMessage()); 
            }
        }

        return $this->render('newUser.html.twig',
        [
            'auth' => $auth, 
            'userLogged' => $user,
            'userExist' => false,
            'newForm' => $form->createView()
        ]);
    }

    /**
    * @Route ("/users_list", name="users_list")
    */

    public function userList(EntityManagerInterface $em, UserManager $us)  //SHOWS A TABLE WITH ALL USERS
    {
        // CHECK IF THERE IS SOME IS SESSION OPEN
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!$auth){
            $this->addFlash(
                'notice',
                'You must be login!!!'
            );
            return $this->redirectToRoute('app_login'); //IF NOT SESSION REDIRECT TO LOGIN SENDING A MESSAGE
         }
        $allUsers = $us->readAllUsers($em);
        return $this->render('showUser.html.twig', 
        [
            'users' => $allUsers, 
            'auth' => $auth, 
            'userLogged' => $user,
            'error' => false
        ]);
    }

    /**
     * @Route ("/modify_user/{id}", name="modify_user")
     */
        
    public function modify($id, //MODIFY USER URL
        EntityManagerInterface $em, 
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder)
    {   
        // CHECK IF THERE IS SOME IS SESSION OPEN
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!$auth){
            $this->addFlash(
                'notice',
                'You must be login!!!'
            );
            return $this->redirectToRoute('app_login');  //IF NOT SESSION REDIRECT TO LOGIN SENDING A MESSAGE
         }
        try
        {
            $repository = $em->getRepository(Usuario::class);
            $usuario = $repository->find($id);
            $form = $this->createForm(ModifyFormType::class, $usuario); //PASS USUARIO TO FILL THE VALUES FIELDS OF THE FORM
            $form->handleRequest($request);
            $data = $form->getData();
            if($form->isSubmitted() && $form->isValid())
            {
                $data = $form->getData();
                $usuario->setPassword($data->getPassword()); 
                $usuario->setEmail($data->getEmail());
                $usuario->setComments($data->getComments());
                $em->persist($usuario);
                $em->flush();
                return $this->redirectToRoute('users_list');
            }
            return $this->render('showUserToModify.html.twig', 
            [
                'user'=> $usuario,
                'auth' => $auth, 
                'userLogged' => $user,
                'modifyForm' => $form->createView()
            ]);
        }catch(Exception $e){
            dd($e->getMessage());
        }
        
    }

    /**
     * @Route ("/delete/{id}", name="delete_user")
     */

    public function deleteUser (EntityManagerInterface $em, $id)
    {
        // CHECK IF THERE IS SOME IS SESSION OPEN
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        if(!$auth){
            $this->addFlash(
                'notice',
                'You must be login!!!'
            );
            return $this->redirectToRoute('app_login'); //IF NOT SESSION REDIRECT TO LOGIN SENDING A MESSAGE
            }
        
        try{
            $repository = $em->getRepository(Usuario::class);
            $logUser = $this->getUser(); 
            $idToDelete = $logUser->getId();
            if($idToDelete == $id){
                return $this->redirectToRoute('users_list');
            }else{
                $user = $repository->find($id);
                $em->remove($user);
                $em->flush();
                return $this->redirectToRoute('users_list');
            }
        }catch(Exception $e){
            dd($e->getMessage()); 
        }
    }

    /**
     * @Route ("/search", name="search")
    */

    public function search (Request $request, EntityManagerInterface $em)
    {
        try{
            $repository = $em->getRepository(Usuario::class);
            $usuario = $repository->findOneBy(['username' => $request->request->get('username')]);
        }
        catch(Exception $e){
          $e->getMessage();
        }   
        if($usuario)
        {
            $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
            $user = $this->getUser();
            return $this->render('searchUser.html.twig', 
            [
                'user'=> $usuario, 
                'auth' => $auth, 
                'userLogged' => $user
            ]);
        }
        return $this->redirectToRoute('users_list', ['error' => true]);
    }
}