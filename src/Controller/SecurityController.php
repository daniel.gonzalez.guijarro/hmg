<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\RegisterFormType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */

    public function login(AuthenticationUtils $authenticationUtils)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('users_list');
        }
        $auth = false;
        $user = new Usuario();
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', 
        [
            'last_username' => $lastUsername,
            'error' => $error,
            'auth' => $auth,
            'userLogged' => $user
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */

    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/registerForm", name="registerForm")
     */

     public function registerForm(
         Request $request,
         UserPasswordEncoderInterface $passwordEncoder,
         EntityManagerInterface $em)
     {
        $user = new usuario();
        $form = $this->createForm(RegisterFormType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
           try{
                $data = $form->getData();
                $user->setUsername($data->getUsername());
                $password = $data->getPassword();
                $passwordEncoder->encodePassword($user, $password);
                $user->setPassword($passwordEncoder->encodePassword($user, $password));
                $user->setEmail($data->getEmail());
                    $em->persist($user);
                    $em->flush();
                    return $this->redirectToRoute('app_login');
            }
            catch (Exception $e) {
                $this->addFlash(
                    'notice',
                    'There are some problems'
                );
                return $this->redirectToRoute('registerForm');
            }
        }

        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        return $this->render('security/register.html.twig',
        [
            'auth' => $auth,
            'userLogged' => $user,
            'password' => true,
            'userExist' => false,
            'registerForm' => $form->createView()
        ]);
     }
}
