<?php

namespace App\Manager;

use App\Entity\Usuario;
use Cloudinary\Cloudinary;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserManager extends AbstractController{

    public function readAllUsers($em){
        try{
            $repository = $em->getRepository(Usuario::class);
            $usuarios = $repository->findAll();
            return $usuarios;
        }
        catch(Exception $e){
            dd();
            $this->addFlash(
                'notice',
                'There are some problems reading the BBDD'
            );
            return $this->redirectToRoute('users_list');
        }
    }

    public function checkIfUserExist ($username, $em)
    {
        try{
            $repository = $em->getRepository(Usuario::class);
            $user = $repository->findOneBy(['username' => $username]); //usuario ó null *
            if(!$user)
            {
                return false;
            }
            return true;
        }
        catch (Exception $e) {
            $this->addFlash(
                'notice',
                'There are some problems whith BBDD'
            );
            return $this->redirectToRoute('users_list');
              
            }
    }
  

  
}