# Instalción del proyecto
    
    git clone https://gitlab.com/daniel.gonzalez.guijarro/hmg.git
    cd hmg
    composer install
        # Esperamos a que se instalen todos los componentes de Symfony. Este paso llevará varios minutos. 
        Be water my friend!

# En windows  

    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate

# En Linux

    bin/console doctrine:database:create
    bin/console doctrine:migrations:migrate

# Levantamos el servidor de Symfony

    symfony serve -d

# Comenzamos...
    
    Pantalla de login y registro. Como no tenemos ningún usuario tenemos que crear uno. Para ellos nos vamos register introducimos los campos obligatorios que son: username, Email y el password.
    
    Una vez introducidos se guarda el nuevo usuario en la BBDD y nos redirige a la pantallad de Login. En ella introducimos el username y la password. Si nos equivocamos aparecerá un mensaje de error.

# Pantalla CRUD

    Una vez logueados tenemos varia información útil en el Header.
        * Imagen del usuario
        * Mensaje de bienvenida al usuario.
        * Botón de logout: Nos desloga de la aplicación y nos devuelve a la pantalla de login.

    Se mostrará una tabla de todos los usuarios dados de alta con sus correspondientes opciones:
        * Modify: Nos lleva a un formulario en el que podemos cambiar todas las opciones del ususario.
            - Username.
            - Comments.
            - Email.
        * Delete: Borra todos los usuarios menos el que está logado. ¡Ojo no pide confirmación!.
        * New: Crea un nuevo usuario.
            - Username: Obligatorio.
            - Email: Obligatorio
            - Password: Obligatorio.
            - Comments: 
        * Search:
            Busca usuarios por el campo username. Si no existe no hace nada. Si existe te muestra en formato tabla el usuario con todas las opciones disponibles.
                - Reset: Si presionamos volvemos a la pantalla inicial.


    
