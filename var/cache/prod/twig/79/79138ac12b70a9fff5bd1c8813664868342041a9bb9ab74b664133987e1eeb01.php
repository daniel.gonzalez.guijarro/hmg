<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUserToModify.html.twig */
class __TwigTemplate_68f7b1416db338232454c19dd66ef16a1fc3731647b407ec95c691c3f2366159 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "showUserToModify.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <div class=\"register__container\">
        <form method=\"post\" action=\"/saveChanges/";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 8), "html", null, true);
        echo "\" enctype=\"multipart/form-data\">
            <h1 class=\"register__title\">Modify form</h1>
            ";
        // line 10
        if ( !twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "image", [], "any", false, false, false, 10)) {
            // line 11
            echo "                <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatar.png"), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 11), "html", null, true);
            echo "\">
            ";
        } else {
            // line 13
            echo "                <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "image", [], "any", false, false, false, 13), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 13), "html", null, true);
            echo "\"> 
            ";
        }
        // line 15
        echo "            
            <input type=\"file\" name=\"image\">
            <div class=\"register__options\">
                <label class=\"register__label\" for=\"inputUsername\">Username</label>
                <input class=\"register__input\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 19), "html", null, true);
        echo "\" type=\"text\" name=\"username\" autofocus>
            </div>
            <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputEmail\">Email</label>
                <input class=\"register__input\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 23), "html", null, true);
        echo "\" type=\"text\" name=\"email\">
            </div>
            <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputPassword\">New password</label>
                <input class=\"register__input\" type=\"password\" value=\"********\" name=\"password\" id=\"inputPassword\">
            </div>
            <div class=\"register__submit\">
             <label for=\"comments\">Comments: </label>
            <textarea class=\"modify__input\" cols=\"40\" rows=\"5\" name=\"comments\">";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "comments", [], "any", false, false, false, 31), "html", null, true);
        echo "</textarea>
            <button class=\"register__button\" type=\"submit\">Modify</button>
            <a class=\"register__back\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "showUserToModify.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 33,  115 => 31,  104 => 23,  97 => 19,  91 => 15,  83 => 13,  75 => 11,  73 => 10,  68 => 8,  65 => 7,  61 => 6,  55 => 4,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "showUserToModify.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUserToModify.html.twig");
    }
}
