<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/register.html.twig */
class __TwigTemplate_563e73c0ffbca614d3508873357b9f249fc5fc7030c93f5017606577f67e7d19 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'error' => [$this, 'block_error'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "security/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Sign Up!";
    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/register.css\">
";
    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "<div class=\"register__main\">
    ";
        // line 10
        $this->displayBlock('error', $context, $blocks);
        // line 24
        echo "    <div class=\"register__container\">
            ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "notice"], "method", false, false, false, 25));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 26
            echo "                <div class=\"flash-notice\">
                    ";
            // line 27
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    
            ";
        // line 33
        echo "        ";
        // line 34
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["registerForm"] ?? null), 'errors');
        echo "
        ";
        // line 36
        echo "            ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["registerForm"] ?? null), 'form_start');
        echo "
            <h1 class=\"register__title\">Please sign up</h1>
            <div class=\"register__options\">
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "username", [], "any", false, false, false, 39), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Username"]);
        echo "
                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "username", [], "any", false, false, false, 40), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "

                ";
        // line 44
        echo "            </div>
            <div class=\"register__options\">
                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "email", [], "any", false, false, false, 46), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "email", [], "any", false, false, false, 47), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                ";
        // line 50
        echo "            </div>
            <div class=\"register__options\">
                ";
        // line 54
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "password", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Password"]);
        echo "
                ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["registerForm"] ?? null), "password", [], "any", false, false, false, 55), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                
            </div>
            ";
        // line 62
        echo "            <div class=\"register__submit\">
                
                ";
        // line 64
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["registerForm"] ?? null), 'form_end');
        echo "
            </div>
            <button class=\"register__button\" type=\"submit\">Sign up</button>
            <a class=\"register__back\" href=\"";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
        echo "\"class=\"register__button\">Back</a>
        </form>
    </div>
</div>
";
    }

    // line 10
    public function block_error($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "        <div class=\"error__messages\">
            ";
        // line 12
        if (($context["userExist"] ?? null)) {
            // line 13
            echo "                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            ";
        }
        // line 17
        echo "            ";
        if ( !($context["password"] ?? null)) {
            // line 18
            echo "                <div class=\"register__error\">
                    <p>Las contraseñas no coinciden </p>
                </div>
            ";
        }
        // line 22
        echo "        </div>
    ";
    }

    public function getTemplateName()
    {
        return "security/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 22,  186 => 18,  183 => 17,  177 => 13,  175 => 12,  172 => 11,  168 => 10,  159 => 67,  153 => 64,  149 => 62,  143 => 55,  138 => 54,  134 => 50,  130 => 47,  126 => 46,  122 => 44,  117 => 40,  113 => 39,  106 => 36,  101 => 34,  99 => 33,  96 => 30,  87 => 27,  84 => 26,  80 => 25,  77 => 24,  75 => 10,  72 => 9,  68 => 8,  60 => 5,  56 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/register.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\security\\register.html.twig");
    }
}
