<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* newUser.html.twig */
class __TwigTemplate_d51ad20fac8fd2341360ceb5fe3b69429476c8111d995923e80c05ddb795c5da extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "newUser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      <div class=\"error__messages\">
            ";
        // line 8
        if ((($context["userExist"] ?? null) == true)) {
            // line 9
            echo "                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            ";
        }
        // line 13
        echo "        </div>
    <div class=\"register__container\">
        <form method=\"post\" action=\"/create_user\" enctype=\"multipart/form-data\">
            <h1 class=\"register__title\">Create form</h1>
            <input type=\"file\" name=\"image\">
            <div class=\"register__options\">
                <label class=\"register__label\" for=\"inputUsername\">Username</label>
                <input class=\"register__input\" type=\"text\" name=\"username\" autofocus>
            </div>
            <div class=\"register__options\">
                <label class=\"register__label\" for=\"inputEmail\">Email</label>
                <input class=\"register__input\" type=\"text\" name=\"email\">
            </div>
            <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputPassword\">Password</label>
                <input class=\"register__input\" type=\"password\" name=\"password\" id=\"inputPassword\">
            </div>
            <div class=\"register__submit\">
             <label for=\"comments\">Comments: </label>
            <textarea class=\"modify__input\" cols=\"40\" rows=\"5\" name=\"comments\"></textarea>
            <button class=\"register__button\" type=\"submit\">Create</button>
            <a class=\"register__back\" href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "newUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 34,  76 => 13,  70 => 9,  68 => 8,  65 => 7,  61 => 6,  55 => 4,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "newUser.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\newUser.html.twig");
    }
}
