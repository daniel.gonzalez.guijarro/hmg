<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/registerForm' => [[['_route' => 'registerForm', '_controller' => 'App\\Controller\\SecurityController::registerForm'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'register', '_controller' => 'App\\Controller\\SecurityController::register'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_user_home', '_controller' => 'App\\Controller\\UserController::home'], null, null, null, false, false, null]],
        '/new' => [[['_route' => 'create_userForm', '_controller' => 'App\\Controller\\UserController::newUser'], null, null, null, false, false, null]],
        '/create_user' => [[['_route' => 'create_user', '_controller' => 'App\\Controller\\UserController::createUser'], null, null, null, false, false, null]],
        '/users_list' => [[['_route' => 'users_list', '_controller' => 'App\\Controller\\UserController::userList'], null, null, null, false, false, null]],
        '/search' => [[['_route' => 'search', '_controller' => 'App\\Controller\\UserController::search'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/modify_user/([^/]++)(*:28)'
                .'|/saveChanges/([^/]++)(*:56)'
                .'|/delete/([^/]++)(*:79)'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        28 => [[['_route' => 'modify_user', '_controller' => 'App\\Controller\\UserController::modify'], ['id'], null, null, false, true, null]],
        56 => [[['_route' => 'saveChanges', '_controller' => 'App\\Controller\\UserController::saveChanges'], ['id'], null, null, false, true, null]],
        79 => [
            [['_route' => 'delete_user', '_controller' => 'App\\Controller\\UserController::deleteUser'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
