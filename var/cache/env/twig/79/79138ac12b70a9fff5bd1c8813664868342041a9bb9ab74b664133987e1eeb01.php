<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUserToModify.html.twig */
class __TwigTemplate_68f7b1416db338232454c19dd66ef16a1fc3731647b407ec95c691c3f2366159 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "showUserToModify.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <div class=\"register__container\">
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "notice"], "method", false, false, false, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 9
            echo "                <div class=\"flash-notice\">
                    ";
            // line 10
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo " 
            ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["modifyForm"] ?? null), 'form_start');
        echo "
                <h1 class=\"register__title\">Modify form</h1>
                ";
        // line 16
        if ( !twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "image", [], "any", false, false, false, 16)) {
            // line 17
            echo "                    <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatar.png"), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 17), "html", null, true);
            echo "\">
                ";
        } else {
            // line 19
            echo "                    <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "image", [], "any", false, false, false, 19), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 19), "html", null, true);
            echo "\"> 
                ";
        }
        // line 21
        echo "              ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "image", [], "any", false, false, false, 21), 'widget', ["attr" => ["type" => "hidden"]]);
        echo "
            <input type=\"file\" name=\"image\">
            <div class=\"register__options\">
                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "username", [], "any", false, false, false, 24), 'label', ["label_attr" => ["class" => "register__label", "value" => "{{user.username}}"], "label" => "Username"]);
        echo "
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "username", [], "any", false, false, false, 25), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "

            </div>
            <div class=\"register__options\">
                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "email", [], "any", false, false, false, 29), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "email", [], "any", false, false, false, 30), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
          
            </div>
            <div class=\"register__options\">
       
               
            </div>
    
             <label for=\"comments\">Comments: </label>
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["modifyForm"] ?? null), "comments", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "modify__input", "cols" => "40", "rows" => "5"]]);
        echo "
     
            <a class=\"register__back\" href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
        ";
        // line 42
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["modifyForm"] ?? null), 'form_end');
        echo "
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "showUserToModify.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 42,  149 => 41,  144 => 39,  132 => 30,  128 => 29,  121 => 25,  117 => 24,  110 => 21,  102 => 19,  94 => 17,  92 => 16,  87 => 14,  84 => 13,  75 => 10,  72 => 9,  68 => 8,  65 => 7,  61 => 6,  55 => 4,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "showUserToModify.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUserToModify.html.twig");
    }
}
