<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUserToModify.html.twig */
class __TwigTemplate_ecdd094f2a9f613e58ffdf6810dc24663c63ba6034d1c65b3b9a2e1f760f0502 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "showUserToModify.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "showUserToModify.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 8
            echo "        <div class=\"flash-notice\">
            ";
            // line 9
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 12, $this->source); })()), 'form_start');
        echo "
    <div class=\"register__container\">
        <h1 class=\"register__title\">Modify form</h1>
        <div class=\"register__options\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 16, $this->source); })()), "username", [], "any", false, false, false, 16), 'label', ["label_attr" => ["class" => "register__label", "value" => "{{user.username}}"], "label" => "Username"]);
        echo "
            ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 17, $this->source); })()), "username", [], "any", false, false, false, 17), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
        </div>
        <div class=\"register__options\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 20, $this->source); })()), "email", [], "any", false, false, false, 20), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 21, $this->source); })()), "email", [], "any", false, false, false, 21), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
        </div>
        <label for=\"comments\">Comments: </label>
        ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 24, $this->source); })()), "comments", [], "any", false, false, false, 24), 'widget', ["attr" => ["class" => "modify__input", "cols" => "40", "rows" => "5"]]);
        echo "
    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 25, $this->source); })()), 'form_end');
        echo "
           <a class=\"register__back\" href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
        
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "showUserToModify.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 26,  129 => 25,  125 => 24,  119 => 21,  115 => 20,  109 => 17,  105 => 16,  97 => 12,  88 => 9,  85 => 8,  80 => 7,  73 => 6,  64 => 4,  60 => 3,  53 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}  
{% block stylesheets %}
{{ parent()}}
    <link rel=\"stylesheet\" href=\"{{ asset ('styles/register.css')}}\">
{% endblock %}
{% block content %}
    {% for message in app.flashes('notice') %}
        <div class=\"flash-notice\">
            {{ message }}
        </div>
    {% endfor %}
    {{ form_start(modifyForm)}}
    <div class=\"register__container\">
        <h1 class=\"register__title\">Modify form</h1>
        <div class=\"register__options\">
            {{ form_label(modifyForm.username, 'Username', { 'label_attr': {'class': 'register__label','value': \"{{user.username}}\" }}) }}
            {{ form_widget(modifyForm.username,  { 'attr': {'class': 'register__input'}})}}
        </div>
        <div class=\"register__options\">
            {{ form_label(modifyForm.email, 'Email', { 'label_attr': {'class': 'register__label'} }) }}
            {{ form_widget(modifyForm.email,  { 'attr': {'class': 'register__input'}})}}
        </div>
        <label for=\"comments\">Comments: </label>
        {{ form_widget(modifyForm.comments,  { 'attr': {'class': 'modify__input','cols':'40', 'rows':'5'}})}}
    {{ form_end(modifyForm)}}
           <a class=\"register__back\" href=\"{{path('users_list')}}\"class=\"register__button\">Back</a>
        
    </div>
{% endblock %}


", "showUserToModify.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUserToModify.html.twig");
    }
}
