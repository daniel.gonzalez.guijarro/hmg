<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUser.html.twig */
class __TwigTemplate_2608a13267de11f93be1122e033a18430b82e8368d712ac7124c3571c741bbf8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "showUser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/userList.css\">
";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "<div class=\"content\">
    ";
        // line 8
        if (($context["error"] ?? null)) {
            // line 9
            echo "        <p>No user found</p>
    ";
        }
        // line 11
        echo "    <div class=\"user__list\">
        <div class=\"user__new\">
            <a class=\"user__button user__button--new\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("create_userForm");
        echo "\">New</a>
            <form method=\"post\" action=\"/search\">
                <img class=\"search__icon\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/icons/loupe.svg"), "html", null, true);
        echo "\" alt=\"\">
                <input class=\"search__input \" type=\"text\" value=\"User name\" name=\"username\" autofocus>
                <button class=\"user__button user__button--search\" type=\"submit\" >Search</button>
            </form>
        </div>
         ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "notice"], "method", false, false, false, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 21
            echo "                <div class=\"flash-notice\">
                    ";
            // line 22
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "        <table class=\"table\">
            <tr class=\"table__header\">
                <th class=\"table__header\">Id</th>
                <th class=\"table__header\">Image</th>
                <th class=\"table__header\">Name</th>
                <th class=\"table__header\">Email</th>
                <th class=\"table__heade\">Comments</th>
                <th></th>
                <th></th>
            </tr>
            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 36
            echo "                ";
            if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 36) % 2) == 0)) {
                // line 37
                echo "                    <tr class=\"table__row\" style=\"background-color: rgb(216, 216, 216);\">
                ";
            } else {
                // line 38
                echo " 
                    <tr class=\"table__row\" style=\"background-color: white;\">
                ";
            }
            // line 41
            echo "                    <td class=\"user__text\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
                    <td>
                    ";
            // line 43
            if (twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 43)) {
                // line 44
                echo "                        <img class=\"user__img\" src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 44), "html", null, true);
                echo "\" alt=\"image of ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 44), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 46
                echo "                        <img class=\"user__img\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    ";
            }
            // line 48
            echo "                    </td>
                    <td class=\"user__text\">";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 49), "html", null, true);
            echo "</td>
                    <td class=\"user__text\">";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 50), "html", null, true);
            echo "</td>
                    <td class=\"user__text-comments\">";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "comments", [], "any", false, false, false, 51), "html", null, true);
            echo "</td>
                    <td class=\"\"><a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("modify_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 52)]), "html", null, true);
            echo "\" class=\"user__button user__button--modify\">Modify</a></td>
                    <td class=\"\"><a href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 53)]), "html", null, true);
            echo "\" class=\"user__button user__button--delete\">Delete</a></td>
            </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </table>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "showUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 56,  184 => 53,  180 => 52,  176 => 51,  172 => 50,  168 => 49,  165 => 48,  161 => 46,  153 => 44,  151 => 43,  145 => 41,  140 => 38,  136 => 37,  133 => 36,  116 => 35,  104 => 25,  95 => 22,  92 => 21,  88 => 20,  80 => 15,  75 => 13,  71 => 11,  67 => 9,  65 => 8,  62 => 7,  58 => 6,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "showUser.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUser.html.twig");
    }
}
