<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/register.html.twig */
class __TwigTemplate_6f87116827b27a1d80df3e30a1bf7b8f5eda207c9ebccdbf87d3bf485c5e08b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'error' => [$this, 'block_error'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/register.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Sign Up!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/register.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "<div class=\"register__main\">
    ";
        // line 10
        $this->displayBlock('error', $context, $blocks);
        // line 24
        echo "    <div class=\"register__container\">
            ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 25));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 26
            echo "                <div class=\"flash-notice\">
                    ";
            // line 27
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 30, $this->source); })()), 'errors');
        echo "
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 31, $this->source); })()), 'form_start');
        echo "
            <h1 class=\"register__title\">Please sign up</h1>
            <div class=\"register__options\">
                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 34, $this->source); })()), "username", [], "any", false, false, false, 34), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Username"]);
        echo "
                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 35, $this->source); })()), "username", [], "any", false, false, false, 35), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__options\">
                ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 38, $this->source); })()), "email", [], "any", false, false, false, 38), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 39, $this->source); })()), "email", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__options\">
                ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 42, $this->source); })()), "password", [], "any", false, false, false, 42), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Password"]);
        echo "
                ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 43, $this->source); })()), "password", [], "any", false, false, false, 43), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__submit\">
                ";
        // line 46
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 46, $this->source); })()), 'form_end');
        echo "
            </div>
            <a class=\"register__back\" href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
        echo "\"class=\"register__button\">Back</a>
   
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_error($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "error"));

        // line 11
        echo "        <div class=\"error__messages\">
            ";
        // line 12
        if ((isset($context["userExist"]) || array_key_exists("userExist", $context) ? $context["userExist"] : (function () { throw new RuntimeError('Variable "userExist" does not exist.', 12, $this->source); })())) {
            // line 13
            echo "                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            ";
        }
        // line 17
        echo "            ";
        if ( !(isset($context["password"]) || array_key_exists("password", $context) ? $context["password"] : (function () { throw new RuntimeError('Variable "password" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "                <div class=\"register__error\">
                    <p>Las contraseñas no coinciden </p>
                </div>
            ";
        }
        // line 22
        echo "        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 22,  196 => 18,  193 => 17,  187 => 13,  185 => 12,  182 => 11,  175 => 10,  163 => 48,  158 => 46,  152 => 43,  148 => 42,  142 => 39,  138 => 38,  132 => 35,  128 => 34,  122 => 31,  117 => 30,  108 => 27,  105 => 26,  101 => 25,  98 => 24,  96 => 10,  93 => 9,  86 => 8,  75 => 5,  68 => 4,  55 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Sign Up!{% endblock %}
{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"styles/register.css\">
{% endblock %}
{% block body %}
<div class=\"register__main\">
    {% block error %}
        <div class=\"error__messages\">
            {% if userExist %}
                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            {% endif %}
            {% if not password %}
                <div class=\"register__error\">
                    <p>Las contraseñas no coinciden </p>
                </div>
            {% endif %}
        </div>
    {% endblock %}
    <div class=\"register__container\">
            {% for message in app.flashes('notice') %}
                <div class=\"flash-notice\">
                    {{ message }}
                </div>
                {% endfor %}
        {{ form_errors(registerForm) }}
            {{ form_start(registerForm) }}
            <h1 class=\"register__title\">Please sign up</h1>
            <div class=\"register__options\">
                {{ form_label(registerForm.username, 'Username', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.username,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__options\">
                {{ form_label(registerForm.email, 'Email', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.email,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__options\">
                {{ form_label(registerForm.password, 'Password', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.password,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__submit\">
                {{ form_end(registerForm) }}
            </div>
            <a class=\"register__back\" href=\"{{path('app_login')}}\"class=\"register__button\">Back</a>
   
    </div>
</div>
{% endblock %}
", "security/register.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\security\\register.html.twig");
    }
}
