<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUser.html.twig */
class __TwigTemplate_d44373e251a362f741425da4552266531c3beb1e36075bd7d488dbfb618dc09a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "showUser.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "showUser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/userList.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "<div class=\"content\">
    ";
        // line 8
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 8, $this->source); })())) {
            // line 9
            echo "        <p>No user found</p>
    ";
        }
        // line 11
        echo "    <div class=\"user__list\">
        <div class=\"user__new\">
            <a class=\"user__button user__button--new\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("create_userForm");
        echo "\">New</a>
            <form method=\"post\" action=\"/search\">
                <img class=\"search__icon\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/icons/loupe.svg"), "html", null, true);
        echo "\" alt=\"\">
                <input class=\"search__input \" type=\"text\" value=\"User name\" name=\"username\" autofocus>
                <button class=\"user__button user__button--search\" type=\"submit\" >Search</button>
            </form>
        </div>
         ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 20, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 21
            echo "                <div class=\"flash-notice\">
                    ";
            // line 22
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "        <table class=\"table\">
            <tr class=\"table__header\">
                <th class=\"table__header\">Id</th>
                <th class=\"table__header\">Image</th>
                <th class=\"table__header\">Name</th>
                <th class=\"table__header\">Email</th>
                <th class=\"table__heade\">Comments</th>
                <th></th>
                <th></th>
            </tr>
            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 35, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 36
            echo "                ";
            if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 36) % 2), 0))) {
                // line 37
                echo "                    <tr class=\"table__row\" style=\"background-color: rgb(216, 216, 216);\">
                ";
            } else {
                // line 38
                echo " 
                    <tr class=\"table__row\" style=\"background-color: white;\">
                ";
            }
            // line 41
            echo "                    <td class=\"user__text\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
                    <td>
                    ";
            // line 43
            if (twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 43)) {
                // line 44
                echo "                        <img class=\"user__img\" src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 44), "html", null, true);
                echo "\" alt=\"image of ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 44), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 46
                echo "                        <img class=\"user__img\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    ";
            }
            // line 48
            echo "                    </td>
                    <td class=\"user__text\">";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 49), "html", null, true);
            echo "</td>
                    <td class=\"user__text\">";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 50), "html", null, true);
            echo "</td>
                    <td class=\"user__text-comments\">";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "comments", [], "any", false, false, false, 51), "html", null, true);
            echo "</td>
                    <td class=\"\"><a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("modify_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 52)]), "html", null, true);
            echo "\" class=\"user__button user__button--modify\">Modify</a></td>
                    <td class=\"\"><a href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 53)]), "html", null, true);
            echo "\" class=\"user__button user__button--delete\">Delete</a></td>
            </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </table>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "showUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 56,  199 => 53,  195 => 52,  191 => 51,  187 => 50,  183 => 49,  180 => 48,  176 => 46,  168 => 44,  166 => 43,  160 => 41,  155 => 38,  151 => 37,  148 => 36,  131 => 35,  119 => 25,  110 => 22,  107 => 21,  103 => 20,  95 => 15,  90 => 13,  86 => 11,  82 => 9,  80 => 8,  77 => 7,  70 => 6,  60 => 3,  53 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\"%}
{% block stylesheets %}
{{ parent()}}
    <link rel=\"stylesheet\" href=\"styles/userList.css\">
{% endblock %}
{% block content %}
<div class=\"content\">
    {% if error %}
        <p>No user found</p>
    {% endif %}
    <div class=\"user__list\">
        <div class=\"user__new\">
            <a class=\"user__button user__button--new\" href=\"{{path('create_userForm')}}\">New</a>
            <form method=\"post\" action=\"/search\">
                <img class=\"search__icon\" src=\"{{ asset ('assets/icons/loupe.svg')}}\" alt=\"\">
                <input class=\"search__input \" type=\"text\" value=\"User name\" name=\"username\" autofocus>
                <button class=\"user__button user__button--search\" type=\"submit\" >Search</button>
            </form>
        </div>
         {% for message in app.flashes('notice') %}
                <div class=\"flash-notice\">
                    {{ message }}
                </div>
                {% endfor %}
        <table class=\"table\">
            <tr class=\"table__header\">
                <th class=\"table__header\">Id</th>
                <th class=\"table__header\">Image</th>
                <th class=\"table__header\">Name</th>
                <th class=\"table__header\">Email</th>
                <th class=\"table__heade\">Comments</th>
                <th></th>
                <th></th>
            </tr>
            {% for user in users %}
                {% if loop.index %2 == 0  %}
                    <tr class=\"table__row\" style=\"background-color: rgb(216, 216, 216);\">
                {% else %} 
                    <tr class=\"table__row\" style=\"background-color: white;\">
                {% endif %}
                    <td class=\"user__text\">{{user.id}}</td>
                    <td>
                    {% if user.image %}
                        <img class=\"user__img\" src=\"{{user.image}}\" alt=\"image of {{user.username}}\">
                    {% else %}
                        <img class=\"user__img\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    {% endif %}
                    </td>
                    <td class=\"user__text\">{{user.username}}</td>
                    <td class=\"user__text\">{{user.email}}</td>
                    <td class=\"user__text-comments\">{{user.comments}}</td>
                    <td class=\"\"><a href=\"{{path('modify_user',{id:user.id})}}\" class=\"user__button user__button--modify\">Modify</a></td>
                    <td class=\"\"><a href=\"{{path('delete_user',{id:user.id})}}\" class=\"user__button user__button--delete\">Delete</a></td>
            </tr>
            {% endfor %}
        </table>
    </div>
</div>
{% endblock %}", "showUser.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUser.html.twig");
    }
}
