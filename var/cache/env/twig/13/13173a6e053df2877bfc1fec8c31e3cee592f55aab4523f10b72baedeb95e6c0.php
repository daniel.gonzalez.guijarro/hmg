<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_fc1dc671b195496d402829e96de91046704c06fb8a8283f9b2563b2124e29668 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "    </head>
    <body class=\"body\">
        ";
        // line 11
        $this->displayBlock('header', $context, $blocks);
        // line 34
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 39
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Hmg CRUD!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/index.css"), "html", null, true);
        echo "\">
        ";
    }

    // line 11
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "            <div class=\"header\">
                <div class=\"header-img__container\">
                    <img class=\"header__img\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/hmg.png"), "html", null, true);
        echo "\" alt=\"logo HMG\">
                </div>
                <h1 class=\"header__title\">HMG USER'S CRUD</h1>
                ";
        // line 17
        if (($context["auth"] ?? null)) {
            // line 18
            echo "                <div class=\"header__logged\">
                    <div>
                    ";
            // line 20
            if (twig_get_attribute($this->env, $this->source, ($context["userLogged"] ?? null), "image", [], "any", false, false, false, 20)) {
                // line 21
                echo "                        <img class=\"header__userimg\" src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userLogged"] ?? null), "image", [], "any", false, false, false, 21), "html", null, true);
                echo "\" alt=\"image of  ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userLogged"] ?? null), "username", [], "any", false, false, false, 21), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 23
                echo "                        <img class=\"header__userimg\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    ";
            }
            // line 25
            echo "                    </div>
                    <div class=\"header__useroption\">
                        <span class=\"header__login\"> Welcome ";
            // line 27
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userLogged"] ?? null), "username", [], "any", false, false, false, 27)), "html", null, true);
            echo "</span>
                        <a class=\"header__logout\"href=\"";
            // line 28
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Logout</a>
                   </div>
                </div>
                ";
        }
        // line 32
        echo "            </div>
        ";
    }

    // line 34
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "            ";
        $this->displayBlock('content', $context, $blocks);
        // line 37
        echo "        ";
    }

    // line 35
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "            ";
    }

    // line 38
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  164 => 38,  160 => 36,  156 => 35,  152 => 37,  149 => 35,  145 => 34,  140 => 32,  133 => 28,  129 => 27,  125 => 25,  121 => 23,  113 => 21,  111 => 20,  107 => 18,  105 => 17,  99 => 14,  95 => 12,  91 => 11,  84 => 7,  80 => 6,  73 => 5,  67 => 39,  64 => 38,  61 => 34,  59 => 11,  55 => 9,  53 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\base.html.twig");
    }
}
