<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUserToModify.html.twig */
class __TwigTemplate_96b136f1f372ee75335d455cbac170fb43b24a76d1b837dfa09231c58c7a7736 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "showUserToModify.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "showUserToModify.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    <div class=\"register__container\">
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 9
            echo "                <div class=\"flash-notice\">
                    ";
            // line 10
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
            ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 14, $this->source); })()), 'form_start');
        echo "
        <form method=\"post\" action=\"/saveChanges/";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15), "html", null, true);
        echo "\" enctype=\"multipart/form-data\">
            <h1 class=\"register__title\">Modify form</h1>
            ";
        // line 17
        if ( !twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "image", [], "any", false, false, false, 17)) {
            // line 18
            echo "                <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatar.png"), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 18, $this->source); })()), "username", [], "any", false, false, false, 18), "html", null, true);
            echo "\">
            ";
        } else {
            // line 20
            echo "                <img class=\"register__image\" src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 20, $this->source); })()), "image", [], "any", false, false, false, 20), "html", null, true);
            echo "\" alt=\"image of ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 20, $this->source); })()), "username", [], "any", false, false, false, 20), "html", null, true);
            echo "\"> 
            ";
        }
        // line 22
        echo "            
            <input type=\"file\" name=\"image\">
            <div class=\"register__options\">
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 25, $this->source); })()), "username", [], "any", false, false, false, 25), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Username"]);
        echo "
                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 26, $this->source); })()), "username", [], "any", false, false, false, 26), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                ";
        // line 29
        echo "            </div>
            <div class=\"register__options\">
                ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 31, $this->source); })()), "email", [], "any", false, false, false, 31), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 32, $this->source); })()), "email", [], "any", false, false, false, 32), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                ";
        // line 35
        echo "            </div>
            <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputPassword\">New password</label>
                <input class=\"register__input\" type=\"password\" value=\"********\" name=\"password\" id=\"inputPassword\">
            </div>
            <div class=\"register__submit\">
             <label for=\"comments\">Comments: </label>
            <textarea class=\"modify__input\" cols=\"40\" rows=\"5\" name=\"comments\">";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "comments", [], "any", false, false, false, 42), "html", null, true);
        echo "</textarea>
            <button class=\"register__button\" type=\"submit\">Modify</button>
            <a class=\"register__back\" href=\"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
        ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["modifyForm"]) || array_key_exists("modifyForm", $context) ? $context["modifyForm"] : (function () { throw new RuntimeError('Variable "modifyForm" does not exist.', 45, $this->source); })()), 'form_end');
        echo "
        </form>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "showUserToModify.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 45,  168 => 44,  163 => 42,  154 => 35,  150 => 32,  146 => 31,  142 => 29,  138 => 26,  134 => 25,  129 => 22,  121 => 20,  113 => 18,  111 => 17,  106 => 15,  102 => 14,  99 => 13,  90 => 10,  87 => 9,  83 => 8,  80 => 7,  73 => 6,  64 => 4,  60 => 3,  53 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}  
{% block stylesheets %}
{{ parent()}}
    <link rel=\"stylesheet\" href=\"{{ asset ('styles/register.css')}}\">
{% endblock %}
{% block content %}
    <div class=\"register__container\">
    {% for message in app.flashes('notice') %}
                <div class=\"flash-notice\">
                    {{ message }}
                </div>
                {% endfor %}

            {{ form_start(modifyForm)}}
        <form method=\"post\" action=\"/saveChanges/{{user.id}}\" enctype=\"multipart/form-data\">
            <h1 class=\"register__title\">Modify form</h1>
            {% if not user.image %}
                <img class=\"register__image\" src=\"{{ asset ('assets/images/avatar.png')}}\" alt=\"image of {{ user.username }}\">
            {% else %}
                <img class=\"register__image\" src=\"{{user.image}}\" alt=\"image of {{ user.username }}\"> 
            {% endif %}
            
            <input type=\"file\" name=\"image\">
            <div class=\"register__options\">
                {{ form_label(registerForm.username, 'Username', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.username,  { 'attr': {'class': 'register__input'}})}}
                {# <label class=\"register__label\" for=\"inputUsername\">Username</label>
                <input class=\"register__input\" value=\"{{user.username}}\" type=\"text\" name=\"username\" autofocus> #}
            </div>
            <div class=\"register__options\">
                {{ form_label(registerForm.email, 'Email', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.email,  { 'attr': {'class': 'register__input'}})}}
                {# <label class=\"register__label\"  for=\"inputEmail\">Email</label>
                <input class=\"register__input\" value=\"{{user.email}}\" type=\"text\" name=\"email\"> #}
            </div>
            <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputPassword\">New password</label>
                <input class=\"register__input\" type=\"password\" value=\"********\" name=\"password\" id=\"inputPassword\">
            </div>
            <div class=\"register__submit\">
             <label for=\"comments\">Comments: </label>
            <textarea class=\"modify__input\" cols=\"40\" rows=\"5\" name=\"comments\">{{ user.comments }}</textarea>
            <button class=\"register__button\" type=\"submit\">Modify</button>
            <a class=\"register__back\" href=\"{{path('users_list')}}\"class=\"register__button\">Back</a>
        {{ form_end(modifyForm)}}
        </form>
    </div>
{% endblock %}", "showUserToModify.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUserToModify.html.twig");
    }
}
