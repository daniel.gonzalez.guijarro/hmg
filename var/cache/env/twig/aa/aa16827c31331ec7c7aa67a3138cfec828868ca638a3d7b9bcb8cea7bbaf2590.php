<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* newUser.html.twig */
class __TwigTemplate_ace5d1859c5adafa203cb72dd257e32cef9a81d0a6c3604eabcad10cef90838d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "newUser.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "newUser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("styles/register.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "      <div class=\"error__messages\">
            ";
        // line 8
        if (((isset($context["userExist"]) || array_key_exists("userExist", $context) ? $context["userExist"] : (function () { throw new RuntimeError('Variable "userExist" does not exist.', 8, $this->source); })()) == true)) {
            // line 9
            echo "                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            ";
        }
        // line 13
        echo "        </div>
         ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 14));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 15
            echo "                <div class=\"flash-notice\">
                    ";
            // line 16
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo " 
    <div class=\"register__container\">
         ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
            <h1 class=\"register__title\">Create form</h1>
          
            <div class=\"register__options\">
               ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 25, $this->source); })()), "username", [], "any", false, false, false, 25), 'label', ["label_attr" => ["class" => "register__label", "value" => "{{user.username}}"], "label" => "Username"]);
        echo "
                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 26, $this->source); })()), "username", [], "any", false, false, false, 26), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__options\">
                 ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 29, $this->source); })()), "email", [], "any", false, false, false, 29), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 30, $this->source); })()), "email", [], "any", false, false, false, 30), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__options\">
                ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 33, $this->source); })()), "password", [], "any", false, false, false, 33), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Password"]);
        echo "
                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 34, $this->source); })()), "password", [], "any", false, false, false, 34), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
            </div>
            <div class=\"register__submit\">
            <label for=\"comments\">Comments: </label>
                ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 38, $this->source); })()), "comments", [], "any", false, false, false, 38), 'widget', ["attr" => ["class" => "modify__input", "cols" => "40", "rows" => "5"]]);
        echo "
          
            <a class=\"register__back\" href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users_list");
        echo "\"class=\"register__button\">Back</a>
       ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["newForm"]) || array_key_exists("newForm", $context) ? $context["newForm"] : (function () { throw new RuntimeError('Variable "newForm" does not exist.', 41, $this->source); })()), 'form_end');
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "newUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 41,  157 => 40,  152 => 38,  145 => 34,  141 => 33,  135 => 30,  131 => 29,  125 => 26,  121 => 25,  114 => 21,  110 => 19,  101 => 16,  98 => 15,  94 => 14,  91 => 13,  85 => 9,  83 => 8,  80 => 7,  73 => 6,  64 => 4,  60 => 3,  53 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}  
{% block stylesheets %}
{{ parent()}}
    <link rel=\"stylesheet\" href=\"{{ asset ('styles/register.css')}}\">
{% endblock %}
{% block content %}
      <div class=\"error__messages\">
            {% if userExist == true %}
                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            {% endif %}
        </div>
         {% for message in app.flashes('notice') %}
                <div class=\"flash-notice\">
                    {{ message }}
                </div>
                {% endfor %}
 
    <div class=\"register__container\">
         {{ form_start(newForm) }}
            <h1 class=\"register__title\">Create form</h1>
          
            <div class=\"register__options\">
               {{ form_label(newForm.username, 'Username', { 'label_attr': {'class': 'register__label','value': \"{{user.username}}\" }}) }}
                {{ form_widget(newForm.username,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__options\">
                 {{ form_label(newForm.email, 'Email', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(newForm.email,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__options\">
                {{ form_label(newForm.password, 'Password', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(newForm.password,  { 'attr': {'class': 'register__input'}})}}
            </div>
            <div class=\"register__submit\">
            <label for=\"comments\">Comments: </label>
                {{ form_widget(newForm.comments,  { 'attr': {'class': 'modify__input','cols':'40', 'rows':'5'}})}}
          
            <a class=\"register__back\" href=\"{{path('users_list')}}\"class=\"register__button\">Back</a>
       {{ form_end(newForm)}}
    </div>
{% endblock %}

", "newUser.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\newUser.html.twig");
    }
}
