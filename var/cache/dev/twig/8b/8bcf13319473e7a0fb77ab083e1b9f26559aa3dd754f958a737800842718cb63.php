<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/register.html.twig */
class __TwigTemplate_35d7257aab6089f9787b24026a6ddfc50fa16482154a68810c37b0325ed9ba5c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'error' => [$this, 'block_error'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/register.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/register.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Sign Up!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/register.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "<div class=\"register__main\">
    ";
        // line 10
        $this->displayBlock('error', $context, $blocks);
        // line 24
        echo "    <div class=\"register__container\">
            ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 25));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 26
            echo "                <div class=\"flash-notice\">
                    ";
            // line 27
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    
            ";
        // line 33
        echo "        ";
        // line 34
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 34, $this->source); })()), 'errors');
        echo "
        ";
        // line 36
        echo "            ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 36, $this->source); })()), 'form_start');
        echo "
            <h1 class=\"register__title\">Please sign up</h1>
            <div class=\"register__options\">
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 39, $this->source); })()), "username", [], "any", false, false, false, 39), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Username"]);
        echo "
                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 40, $this->source); })()), "username", [], "any", false, false, false, 40), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "

                ";
        // line 44
        echo "            </div>
            <div class=\"register__options\">
                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 46, $this->source); })()), "email", [], "any", false, false, false, 46), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Email"]);
        echo "
                ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 47, $this->source); })()), "email", [], "any", false, false, false, 47), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                ";
        // line 50
        echo "            </div>
            <div class=\"register__options\">
                ";
        // line 54
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 54, $this->source); })()), "password", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "register__label"], "label" => "Password"]);
        echo "
                ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 55, $this->source); })()), "password", [], "any", false, false, false, 55), 'widget', ["attr" => ["class" => "register__input"]]);
        echo "
                
            </div>
            ";
        // line 62
        echo "            <div class=\"register__submit\">
                
                ";
        // line 64
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 64, $this->source); })()), 'form_end');
        echo "
            </div>
            <button class=\"register__button\" type=\"submit\">Sign up</button>
            <a class=\"register__back\" href=\"";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
        echo "\"class=\"register__button\">Back</a>
        </form>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_error($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "error"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "error"));

        // line 11
        echo "        <div class=\"error__messages\">
            ";
        // line 12
        if ((isset($context["userExist"]) || array_key_exists("userExist", $context) ? $context["userExist"] : (function () { throw new RuntimeError('Variable "userExist" does not exist.', 12, $this->source); })())) {
            // line 13
            echo "                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            ";
        }
        // line 17
        echo "            ";
        if ( !(isset($context["password"]) || array_key_exists("password", $context) ? $context["password"] : (function () { throw new RuntimeError('Variable "password" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "                <div class=\"register__error\">
                    <p>Las contraseñas no coinciden </p>
                </div>
            ";
        }
        // line 22
        echo "        </div>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 22,  240 => 18,  237 => 17,  231 => 13,  229 => 12,  226 => 11,  216 => 10,  201 => 67,  195 => 64,  191 => 62,  185 => 55,  180 => 54,  176 => 50,  172 => 47,  168 => 46,  164 => 44,  159 => 40,  155 => 39,  148 => 36,  143 => 34,  141 => 33,  138 => 30,  129 => 27,  126 => 26,  122 => 25,  119 => 24,  117 => 10,  114 => 9,  104 => 8,  90 => 5,  80 => 4,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Sign Up!{% endblock %}
{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"styles/register.css\">
{% endblock %}
{% block body %}
<div class=\"register__main\">
    {% block error %}
        <div class=\"error__messages\">
            {% if userExist %}
                <div class=\"register__error\">
                    <p>El usuario ya existe</p>
                </div>
            {% endif %}
            {% if not password %}
                <div class=\"register__error\">
                    <p>Las contraseñas no coinciden </p>
                </div>
            {% endif %}
        </div>
    {% endblock %}
    <div class=\"register__container\">
            {% for message in app.flashes('notice') %}
                <div class=\"flash-notice\">
                    {{ message }}
                </div>
                {% endfor %}
    
            {# {{ form_label(registerForm.username, 'Username', { 'label_attr': {'class': 'register__label'} }) }}
            {{ form_widget(registerForm.username,  { 'attr': {'class': 'register__input'}})}} #}
        {# {{ form_end(registerForm)}} #}
        {{ form_errors(registerForm) }}
        {# <form method=\"post\" action=\"{{path('register')}}\"> #}
            {{ form_start(registerForm) }}
            <h1 class=\"register__title\">Please sign up</h1>
            <div class=\"register__options\">
                {{ form_label(registerForm.username, 'Username', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.username,  { 'attr': {'class': 'register__input'}})}}

                {# <label class=\"register__label\" for=\"inputUsername\">Username</label>
                <input class=\"register__input\" type=\"text\" name=\"username\"  required autofocus> #}
            </div>
            <div class=\"register__options\">
                {{ form_label(registerForm.email, 'Email', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.email,  { 'attr': {'class': 'register__input'}})}}
                {# <label class=\"register__label\"  for=\"inputEmail\">Email</label>
                <input class=\"register__input\" type=\"text\" name=\"email\"  required> #}
            </div>
            <div class=\"register__options\">
                {# <label class=\"register__label\"  for=\"inputPassword\">Password</label>
                <input class=\"register__input\" type=\"password\" name=\"password\" id=\"inputPassword\" required> #}
                {{ form_label(registerForm.password, 'Password', { 'label_attr': {'class': 'register__label'} }) }}
                {{ form_widget(registerForm.password,  { 'attr': {'class': 'register__input'}})}}
                
            </div>
            {# <div class=\"register__options\">
                <label class=\"register__label\"  for=\"inputPassword\">Repeat password</label>
                <input class=\"register__input\" type=\"password\" name=\"repeatPassword\" required>
            </div> #}
            <div class=\"register__submit\">
                
                {{ form_end(registerForm) }}
            </div>
            <button class=\"register__button\" type=\"submit\">Sign up</button>
            <a class=\"register__back\" href=\"{{path('app_login')}}\"class=\"register__button\">Back</a>
        </form>
    </div>
</div>
{% endblock %}
", "security/register.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\security\\register.html.twig");
    }
}
