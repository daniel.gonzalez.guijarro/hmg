<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* showUser.html.twig */
class __TwigTemplate_57a0ca4498fa9a44b6d3829f297916e763f651b3d00d31d4fdbd9f903a2e2272 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "showUser.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "showUser.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "showUser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"styles/userList.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "<div class=\"content\">
    ";
        // line 8
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 8, $this->source); })())) {
            // line 9
            echo "        <p>No user found</p>
    ";
        }
        // line 11
        echo "    <div class=\"user__list\">
        <div class=\"user__new\">
            <a class=\"user__button user__button--new\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("create_userForm");
        echo "\">New</a>
            <form method=\"post\" action=\"/search\">
                <img class=\"search__icon\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/icons/loupe.svg"), "html", null, true);
        echo "\" alt=\"\">
                <input class=\"search__input \" type=\"text\" value=\"User name\" name=\"username\" autofocus>
                <button class=\"user__button user__button--search\" type=\"submit\" >Search</button>
            </form>
        </div>
        <table class=\"table\">
            <tr class=\"table__header\">
                <th class=\"table__header\">Id</th>
                <th class=\"table__header\">Image</th>
                <th class=\"table__header\">Name</th>
                <th class=\"table__header\">Email</th>
                <th class=\"table__heade\">Comments</th>
                <th></th>
                <th></th>
            </tr>
            ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 30, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 31
            echo "                ";
            if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 31) % 2) == 0)) {
                // line 32
                echo "                    <tr class=\"table__row\" style=\"background-color: rgb(216, 216, 216);\">
                ";
            } else {
                // line 33
                echo " 
                    <tr class=\"table__row\" style=\"background-color: white;\">
                ";
            }
            // line 36
            echo "                    <td class=\"user__text\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 36), "html", null, true);
            echo "</td>
                    <td>
                    ";
            // line 38
            if (twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 38)) {
                // line 39
                echo "                        <img class=\"user__img\" src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "image", [], "any", false, false, false, 39), "html", null, true);
                echo "\" alt=\"image of ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 39), "html", null, true);
                echo "\">
                    ";
            } else {
                // line 41
                echo "                        <img class=\"user__img\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    ";
            }
            // line 43
            echo "                    </td>
                    <td class=\"user__text\">";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 44), "html", null, true);
            echo "</td>
                    <td class=\"user__text\">";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 45), "html", null, true);
            echo "</td>
                    <td class=\"user__text-comments\">";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "comments", [], "any", false, false, false, 46), "html", null, true);
            echo "</td>
                    <td class=\"\"><a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("modify_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\" class=\"user__button user__button--modify\">Modify</a></td>
                    <td class=\"\"><a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 48)]), "html", null, true);
            echo "\" class=\"user__button user__button--delete\">Delete</a></td>
            </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "        </table>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "showUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 51,  196 => 48,  192 => 47,  188 => 46,  184 => 45,  180 => 44,  177 => 43,  173 => 41,  165 => 39,  163 => 38,  157 => 36,  152 => 33,  148 => 32,  145 => 31,  128 => 30,  110 => 15,  105 => 13,  101 => 11,  97 => 9,  95 => 8,  92 => 7,  82 => 6,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\"%}
{% block stylesheets %}
{{ parent()}}
    <link rel=\"stylesheet\" href=\"styles/userList.css\">
{% endblock %}
{% block content %}
<div class=\"content\">
    {% if error %}
        <p>No user found</p>
    {% endif %}
    <div class=\"user__list\">
        <div class=\"user__new\">
            <a class=\"user__button user__button--new\" href=\"{{path('create_userForm')}}\">New</a>
            <form method=\"post\" action=\"/search\">
                <img class=\"search__icon\" src=\"{{ asset ('assets/icons/loupe.svg')}}\" alt=\"\">
                <input class=\"search__input \" type=\"text\" value=\"User name\" name=\"username\" autofocus>
                <button class=\"user__button user__button--search\" type=\"submit\" >Search</button>
            </form>
        </div>
        <table class=\"table\">
            <tr class=\"table__header\">
                <th class=\"table__header\">Id</th>
                <th class=\"table__header\">Image</th>
                <th class=\"table__header\">Name</th>
                <th class=\"table__header\">Email</th>
                <th class=\"table__heade\">Comments</th>
                <th></th>
                <th></th>
            </tr>
            {% for user in users %}
                {% if loop.index %2 == 0  %}
                    <tr class=\"table__row\" style=\"background-color: rgb(216, 216, 216);\">
                {% else %} 
                    <tr class=\"table__row\" style=\"background-color: white;\">
                {% endif %}
                    <td class=\"user__text\">{{user.id}}</td>
                    <td>
                    {% if user.image %}
                        <img class=\"user__img\" src=\"{{user.image}}\" alt=\"image of {{user.username}}\">
                    {% else %}
                        <img class=\"user__img\" src=\"assets/images/avatar.png\" alt=\"image of an avatar\">
                    {% endif %}
                    </td>
                    <td class=\"user__text\">{{user.username}}</td>
                    <td class=\"user__text\">{{user.email}}</td>
                    <td class=\"user__text-comments\">{{user.comments}}</td>
                    <td class=\"\"><a href=\"{{path('modify_user',{id:user.id})}}\" class=\"user__button user__button--modify\">Modify</a></td>
                    <td class=\"\"><a href=\"{{path('delete_user',{id:user.id})}}\" class=\"user__button user__button--delete\">Delete</a></td>
            </tr>
            {% endfor %}
        </table>
    </div>
</div>
{% endblock %}", "showUser.html.twig", "C:\\Users\\yamuw\\Documents\\Programacion\\PHP\\hmg\\templates\\showUser.html.twig");
    }
}
