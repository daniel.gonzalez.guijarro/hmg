<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXTOxIfU\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXTOxIfU/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerXTOxIfU.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerXTOxIfU\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerXTOxIfU\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'XTOxIfU',
    'container.build_id' => 'c497ed65',
    'container.build_time' => 1618329630,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerXTOxIfU');
